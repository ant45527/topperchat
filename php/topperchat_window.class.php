<?php

/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 11/16/2016
 * Time: 9:26 AM
 */

include_once "sqler.class.php";
include_once "conversation_box.class.php";

class topperchat_window
{
    // For maintaining this user's conversations
    protected $currentUsersConversationIds;
    protected $new;

    public function __construct($new = FALSE) {
        // Initialize fields, etc.
        $this->new = $new;
    }

    public function __toString()
    {
        return $this->buildConversationHistory() . $this->buildChatWindow() . $this->buildLogoutButton() . $this->buildUserIdLabel();
    }

    private function buildChatWindow() {
        $sendButton = "<button id='send_button' onclick='sendMessage()'>Send</button>";
        $messageBox = "<textarea id='message_box'></textarea>";
        $replyContainer = "<div id='reply_container'>" . $messageBox . $sendButton . "</div>";

        if (!$this->new && isset($_SESSION["id_conversation"]) && !is_null($_SESSION["id_conversation"])) {
            $messages = self::loadMessagesForConversation($_SESSION["id_conversation"]);
            $allParticipants = self::getRecipientsForConversation($_SESSION["id_conversation"]);
            $recipientsContainer = "<div id='recipients_container'><button id='to_button' disabled>To</button><input id='recipients' type='text' value='$allParticipants' disabled></div>";
            $container = "<div id='chat_window'>" . $messages . "</div>";
            return $recipientsContainer . $container . $replyContainer;
        }
        else {
            $recipientsContainer = "<div id='recipients_container'><button id='to_button' onclick='showSearchUsersPopup();'>To</button><input id='recipients' type='text'></div>";
            $container = "<div id='chat_window'></div>";
            return $recipientsContainer . $container . $replyContainer;
        }
    }

    private function buildConversationHistory() {
        $this->currentUsersConversationIds = self::getConversationIdsForUserId();
        if (!$this->new && (!isset($_SESSION["id_conversation"]) || $_SESSION["id_conversation"] == NULL) && !empty($this->currentUsersConversationIds)) {
            $_SESSION["id_conversation"] = $this->currentUsersConversationIds[0];
        }

        $conversationBoxes = [];
        foreach($this->currentUsersConversationIds as $id) {
            if (!$this->new && $_SESSION["id_conversation"] == $id) {
                $conversationBoxes[] = self::createConversationBoxForConversationId($id, TRUE);
            }
            else {
                $conversationBoxes[] = self::createConversationBoxForConversationId($id);
            }
        }

        $conversationWindowLabel = "<p id='conversations_window_label'>Conversations</p>";
        $conversationsList = "<div id='conversations_list'>" . implode("", $conversationBoxes) . "</div>";
        $newConversationButton = "<button id='new_conversation_button' onclick='newConversation();'>+</button>";
        $container = "<div id='conversations_window'>" . $conversationWindowLabel . $newConversationButton . $conversationsList . "</div>";
        return $container;
    }

    private function buildLogoutButton() {
        $logoutButton = "<button id='logout_button' onclick='logout();'><p>Logout</p></button>";
        return $logoutButton;
    }

    private function buildUserIdLabel() {
        $name = self::getNameForUserId($_SESSION["id_user"], FALSE);

        $container = "<div title='Current User is $name' id='user_id_label'>";
        $container .= "<p id='user_name_label'>$name</p>";
        $container .= "<p id='user_email_label'>" . self::getEmailForUserId($_SESSION["id_user"], FALSE) . "</p>";
        $container .= "</div>";
        return $container;
    }

    // Gets the conversation ids for this user and returns them in an array
    public static function getConversationIdsForUserId() {
        $userId = $_SESSION["id_user"];
        $sqler = new sqler();
        $sqler->sendQuery("Select id from conversation where creator='$userId' or participant_1='$userId'
                            or participant_2='$userId' or participant_3='$userId'
                            or participant_4='$userId' or participant_5='$userId'");
        $conversationIds = [];
        while ($row = $sqler->getRow()) {
            $conversationIds[] = $row["id"];
        }

        if (empty($conversationIds)) {
            return [];
        }
        return $conversationIds;
    }

    // Creates a new conversation box for the given id and returns the string representing the HTML
    public static function createConversationBoxForConversationId($id, $selected = FALSE) {
        $sqler = new sqler();
        $sqler->sendQuery("Select * from conversation where id=$id");

        if ($row = $sqler->getRow()) {
            $allParticipants = [self::getNameForUserId($row["creator"]), self::getNameForUserId($row["participant_1"])];
            if ($row["participant_2"] != null) {
                $name = self::getNameForUserId($row["participant_2"]);
                $allParticipants[] = $name;
            }
            if ($row["participant_3"] != null) {
                $name = self::getNameForUserId($row["participant_3"]);
                $allParticipants[] = $name;
            }
            if ($row["participant_4"] != null) {
                $name = self::getNameForUserId($row["participant_4"]);
                $allParticipants[] = $name;
            }
            if ($row["participant_5"] != null) {
                $name = self::getNameForUserId($row["participant_5"]);
                $allParticipants[] = $name;
            }
            return new Conversation_Box($id, $allParticipants, $selected);
        }
        else {
            return null;
        }
    }

    public static function getNameForUserId($id, $useYou = TRUE) {
        if ($useYou && $id == $_SESSION["id_user"]) {
            return "You";
        }

        $sqler = new sqler();
        $sqler->sendQuery("Select email from user where id=$id");
        if ($row = $sqler->getRow()) {
            $email = $row["email"];
            $emailPreAt = substr($email, 0, strpos($email, '@'));
            $nameWithSpaces = str_replace(".", " ", $emailPreAt);
            $nameWithoutNumsAndUpCased = ucwords(preg_replace('/[0-9]+/', '', $nameWithSpaces));
            return $nameWithoutNumsAndUpCased;
        }
        else {
            return null;
        }
    }

    public static function loadMessagesForConversation($id) {
        $sqler = new sqler();
        $sqler->sendQuery("Select * from message where conversation_id=$id");
        $messages = [];
        while ($row = $sqler->getRow()) {
            $contents = $row["contents"];
            $id = $row["id"];
            if ($row["sender"] == $_SESSION["id_user"]) {
                $messages[] = "<div data-id=$id class='message message_from_self'><p>$contents</p></div>";
            }
            else {
                $messages[] = "<div data-id=$id class='message message_from_other'><p>$contents<br><span style='margin: 3px; float:right; font-size: 8pt; font-style: italic;'>" . self::getNameForUserId($row["sender"]) . "</span></p></div>";
            }
        }
        return implode("", $messages);
    }

    public static function getEmailForUserId($id) {
        $sqler = new sqler();
        $sqler->sendQuery("Select email from user where id=$id");
        if ($row = $sqler->getRow()) {
            return $row["email"];
        }
        else {
            return null;
        }
    }

    public static function getRecipientsForConversation($id) {
        $sqler = new sqler();
        $sqler->sendQuery("Select * from conversation where id=$id");

        if ($row = $sqler->getRow()) {
            $allParticipants = [self::getEmailForUserId($row["creator"]), self::getEmailForUserId($row["participant_1"])];
            if ($row["participant_2"] != null) {
                $allParticipants[] = self::getEmailForUserId($row["participant_2"]);
            }
            if ($row["participant_3"] != null) {
                $allParticipants[] = self::getEmailForUserId($row["participant_3"]);
            }
            if ($row["participant_4"] != null) {
                $allParticipants[] = self::getEmailForUserId($row["participant_4"]);
            }
            if ($row["participant_5"] != null) {
                $allParticipants[] = self::getEmailForUserId($row["participant_5"]);
            }
            return implode("; ", $allParticipants);
        }
        else {
            return null;
        }
    }
}