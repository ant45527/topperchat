<?php
/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 11/25/2016
 * Time: 12:53 PM
 */

include_once "../php/sqler.class.php";
include_once "../php/topperchat_window.class.php";

session_start();

$lastForeignMessageId = filter_input(INPUT_POST, "lastForeignMessageId", FILTER_SANITIZE_NUMBER_INT);
$conversationId = filter_input(INPUT_POST, "conversationId", FILTER_SANITIZE_NUMBER_INT);
$userId = $_SESSION["id_user"];

if (isset($_SESSION["id_conversation"]) && !is_null($_SESSION["id_conversation"])) {
    $conversationId = $_SESSION["id_conversation"];
}

if (!isset($conversationId) || is_null($conversationId) || $conversationId == 0) {
    echo 0;
    return;
}

$sqler = new sqler();

if (!isset($lastForeignMessageId) || is_null($lastForeignMessageId) || $lastForeignMessageId == 0) {
    $sqler->sendQuery("Select `timestamp` from message where message.conversation_id=$conversationId");
}
else {
    $sqler->sendQuery("Select `timestamp` from message where message.id=$lastForeignMessageId and message.conversation_id=$conversationId");
}

if ($row = $sqler->getRow()) {
    $timestamp = $row["timestamp"];
    $sqler->sendQuery("Select * from message where message.`timestamp` > '" . $timestamp . "' and message.conversation_id=$conversationId");
    $messagesToAppend = [];
    while ($row = $sqler->getRow()) {
        $contents = $row["contents"];
		$messageId = $row["id"];
        if (intval($row["sender"]) == $userId) {
            $messagesToAppend[] = "<div data-id=$messageId class='message message_from_self'><p>$contents</p></div>";
        }
        else {
            $messagesToAppend[] = "<div data-id=$messageId class='message message_from_other'><p>$contents<br><span style='margin-top:3px; float:right; font-size: 8pt; font-style: italic;'>" . topperchat_window::getNameForUserId($row["sender"]) . "</span></p></div>";
        }
    }
    echo implode("", $messagesToAppend);
}
else {
    echo 0;
}
