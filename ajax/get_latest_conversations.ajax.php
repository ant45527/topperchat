<?php
/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 11/25/2016
 * Time: 12:53 PM
 */

include_once "../php/sqler.class.php";
include_once "../php/conversation_box.class.php";
include_once "../php/topperchat_window.class.php";

session_start();

$lastConversationId = filter_input(INPUT_POST, "lastConversationId", FILTER_SANITIZE_NUMBER_INT);
$userId = $_SESSION["id_user"];

$sqler = new sqler();

if ($lastConversationId == null) {
    $sqler->sendQuery("Select * from conversation where (creator = $userId or participant_1 = $userId or participant_2 = $userId or participant_3 = $userId or participant_4 = $userId or participant_5 = $userId)");
}
else {
    $sqler->sendQuery("Select * from conversation where id > $lastConversationId and (creator = $userId or participant_1 = $userId or participant_2 = $userId or participant_3 = $userId or participant_4 = $userId or participant_5 = $userId)");
}

if ($row = $sqler->getRow()) {
	$conversationsToAppend = [];
	
	while ($row) {
		$allParticipants = [];
		$id = $row["id"];
		
		$allParticipants[] = $row["creator"];
		$allParticipants[] = $row["participant_1"];
		
		if (isset($row["participant_2"]) && !is_null($row["participant_2"])) {
			$allParticipants[] = $row["participant_2"];
			
		}
		if (isset($row["participant_3"]) && !is_null($row["participant_3"])) {
			$allParticipants[] = $row["participant_3"];
		}
		if (isset($row["participant_4"]) && !is_null($row["participant_4"])) {
			$allParticipants[] = $row["participant_4"];
		}
		if (isset($row["participant_5"]) && !is_null($row["participant_5"])) {
			$allParticipants[] = $row["participant_5"];
		}
		
		$conversationsToAppend[] = topperchat_window::createConversationBoxForConversationId($id);
		$row = $sqler->getRow();
	}
	
	echo implode("", $conversationsToAppend);
}
else {
	echo 0;
}


