<?php
/**
 * Created by PhpStorm.
 * User: ant45527
 * Date: 11/18/2016
 * Time: 12:37 PM
 */

include "../php/sqler.class.php";
include "../php/topperchat_window.class.php";

session_start();

$sqler = new sqler();

$text = filter_input(INPUT_POST, "text", FILTER_SANITIZE_STRING);
$conversationId = null;

if (isset($_SESSION["id_conversation"])) {
    $conversationId = $_SESSION["id_conversation"];
}


$recipients = filter_input(INPUT_POST, "recipients", FILTER_SANITIZE_STRING);
$recipientsAsArray = explode("; ", $recipients);

$userId = $_SESSION["id_user"];

// Check the quality of each of the recipients and echo the issues to be alerted to the user via JS
// Also get the user ids that match each of the emails
$count = 1;
$recipientIds = array_fill(0, 5, null);
foreach ($recipientsAsArray as $recipient) {
    // Check for bad recipients (not in the database)
    $sqler->sendQuery("Select id from user where user.email='$recipient'");
    $row = $sqler->getRow();
    if (!$row) {
        echo "There is no Topperchat account associated with the email provided as recipient number $count!";
        return;
    }
    else if ($row["id"] == $_SESSION["id_user"] && $conversationId == null) {
        echo "You cannot send a message to yourself! Please remove your email address from the recipients box.";
        return;
    }
    else {
        $recipientIds[$count-1] = $row["id"];
    }
    $count++;
}
$recipientIds = array_values($recipientIds);

if (isset($conversationId) && !is_null($conversationId)) { // Add new message to old conversation
    if(!$stmt = $sqler->con->prepare("INSERT INTO message (conversation_id, sender, contents) VALUES (?,?,?)"))
    {
        echo "Prepare fail (" . $sqler->con->errno . ") " . $sqler->con->error;
    }

    if(!$stmt->bind_param("iis", $conversationId, $userId, $text))
    {
        echo "Bind fail (" . $stmt->errno . ") " . $stmt->error;
    }
    if($stmt->execute())
    {
        $messageId = $stmt->insert_id;
        $stmt->close();
        echo json_encode(["messageId" => $messageId]); // Success
    }
    else {
        $error = "Execute fail (" . $stmt->errno . ") " . $stmt->error; // Print the error
        $stmt->close();
        echo $error;
    }
}
else { // Create a new conversation then new message
    if(!$stmt = $sqler->con->prepare("INSERT INTO conversation (creator, participant_1, participant_2, participant_3, participant_4, participant_5) VALUES (?,?,?,?,?,?)"))
    {
        echo "Prepare fail (" . $sqler->con->errno . ") " . $sqler->con->error;
    }

    $creator = $userId;
    $participant1 = $recipientIds[0];
    $participant2 = $recipientIds[1];
    $participant3 = $recipientIds[2];
    $participant4 = $recipientIds[3];
    $participant5 = $recipientIds[4];

    if(!$stmt->bind_param("iiiiii", $creator, $participant1,  $participant2, $participant3, $participant4, $participant5))
    {
        echo "Bind fail (" . $stmt->errno . ") " . $stmt->error;
    }

    if($stmt->execute())
    {
        $conversationId = $stmt->insert_id;
        $stmt->close();
        if(!$stmt = $sqler->con->prepare("INSERT INTO message (conversation_id, sender, contents) VALUES (?,?,?)"))
        {
            echo "Prepare fail (" . $sqler->con->errno . ") " . $sqler->con->error;
        }

        if(!$stmt->bind_param("iis", $conversationId, $userId, $text))
        {
            echo "Bind fail (" . $stmt->errno . ") " . $stmt->error;
        }
        if($stmt->execute())
        {
            $messageId = $stmt->insert_id;
            $stmt->close();
            // Update the session id conversation variable to track the chat window's conversation contents
            $_SESSION["id_conversation"] = $conversationId;
            $conversationBox = "" . topperchat_window::createConversationBoxForConversationId($conversationId, TRUE);
            $senderEmail = topperchat_window::getEmailForUserId($_SESSION["id_user"]);
            echo json_encode(["success" => 1, "boxToAppend" => $conversationBox, "messageId" => $messageId, "senderEmail" => $senderEmail]); // Success
        }
        else {
            $error = "Execute fail (" . $stmt->errno . ") " . $stmt->error; // Print the error
            $stmt->close();
            echo $error;
        }
    }
    else
    {
        $error = "Execute fail (" . $stmt->errno . ") " . $stmt->error; // Print the error
        $stmt->close();
        echo $error;
    }
}
