<?php
/**
 * Created by PhpStorm.
 * User: Drew
 * Date: 11/25/2016
 * Time: 11:58 AM
 */

include_once "../php/topperchat_window.class.php";

session_start();

$conversationId = filter_input(INPUT_POST, "conversationId", FILTER_SANITIZE_STRING);

$_SESSION["id_conversation"] = $conversationId;

echo new topperchat_window();