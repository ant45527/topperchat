var searchTimer = null; // the timer to allow a delay between the input and database queries for searching
var searching = false; // boolean maintaining the state of the search

var messageRequestTimer = null; // the timer to allow a delay between each message request
var conversationRequestTimer = null; // the timer to allow a delay between each conversation request

// Load these functions on page load
$(document).ready(function() {
    loadAllBackgroundFunctions()
});

// Loads all the background functions
function loadAllBackgroundFunctions() {
    loadEnterListener();
    if ($("#chat_window").length != 0) {
        loadMessageRequester();
    }
	if ($("#conversations_list").length != 0) {
		loadConversationRequester();
	}
}

// Load the 'enter' listener and specify the handler
function loadEnterListener() {
    // Catch keypresses to allow quick input interactions
    $("textarea, input").keypress(function(event) {
        // Catch enter key presses
        if(event.which == 13) {
            // Prevent the default mechanism
            event.preventDefault();
            console.log("Caught 'enter' keypress event!");
            // Get the focused input
            var focused = $(':focus');
            // If one exists
            if ($(focused).length != 0) {
                console.log("Found a nearby button and clicked it...");
                // Click the closest button
                $(focused).siblings("button").click();
            }
        }
    });
}

// Loads the function for repeatedly requesting the latest messages
function loadMessageRequester() {
    console.log("Loading message requester!");
    if (messageRequestTimer != null) {
        clearInterval(messageRequestTimer);
        messageRequestTimer = null;
    }
    messageRequestTimer = setInterval(function() {
        console.log("Checking for new messages!");
        var lastForeignMessageId = parseInt($(".message_from_other").last().attr("data-id"));
        var conversationId = parseInt($(".selected_conversation").first().attr("data-id"));

		$.ajax({
            url: 'ajax/get_latest_messages.ajax.php',
            type: 'POST',
            data: {
                AJAX: true,
                conversationId: conversationId,
                lastForeignMessageId: lastForeignMessageId
            },
            success: function(data) {
                if (data != 0) {
                    console.log("Retrieved latest messages successfully!");
                    var toBeReplaced = $(".message_from_other").last().nextAll();
                    $(toBeReplaced).remove();
                    $("#chat_window").append(data);
					// Scroll to the bottom of the chat window
					if (strlen(data) != 0) {
						$("#chat_window").animate({scrollTop: $("#chat_window").prop("scrollHeight")}, 'slow');
					}
                }
            },
            error: function() {
                console.log("Error with retrieving latest messages!!");
            }
        });
    }, 1000);
}

// Loads the function for repeatedly requesting the latest conversations
function loadConversationRequester() {
	console.log("Loading conversation requester!");
	if (conversationRequestTimer != null) {
        clearInterval(conversationRequestTimer);
        conversationRequestTimer = null;
    }
    conversationRequestTimer = setInterval(function() {
        console.log("Checking for new conversations!");
        var lastConversationId = parseInt($(".conversation_box").last().attr("data-id"));
		$.ajax({
            url: 'ajax/get_latest_conversations.ajax.php',
            type: 'POST',
            data: {
                AJAX: true,
                lastConversationId: lastConversationId
            },
            success: function(data) {
                if (data != 0) {
                    console.log("Retrieved latest conversations successfully!");
                    $("#conversations_list").append(data);
                }
				else {
					console.log("Error with retrieving latest conversations!");
				}
            },
            error: function() {
                console.log("Error with retrieving latest conversations!");
            }
        });
    }, 5000);
}

// Try to log in with the login form data
function login() {
    var email = $("#login_email").val();
    var password = $("#login_password").val();

    clearInvalidInputNotifiers();

    if (email.length === 0) {
        $("#login_email_label").css({ 'color': 'red'});
        $("#invalid_login_message").text("Please enter your email!");
        return;
    }

    if (password.length === 0) {
        $("#login_pass_label").css({ 'color': 'red'});
        $("#invalid_login_message").text("Please enter your password!");
        return;
    }

    $.ajax({
        url: 'ajax/login.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            email: (email),
            password: (password)
        },
        success: function(data) {
            if (data == 1) {
                console.log("Login successful!");
                window.location.reload();
            }
            else {
                $("#login_email_label").css({ 'color': 'red'});
                $("#login_pass_label").css({ 'color': 'red'});
                $("#invalid_login_message").text("Invalid credentials!");
            }
        },
        error: function() {
            console.log("Error with login!");
        }
    });
}

// Try to register with the register form data
function register() {
    var email = $("#register_email").val();
    var password = $("#register_password").val();
    var confirmPass = $("#confirm_password").val();

    clearInvalidInputNotifiers();

    if (email.length === 0) {
        $("#register_email_label").css({ 'color': 'red'});
        $("#invalid_registration_message").text("Your email cannot be empty!");
        return;
    }

    if (!email.includes("wku.edu")) {
        $("#register_email_label").css({'color': 'red'});
        $("#invalid_registration_message").text("Your email must be a Toppermail account!");
        return;
    }

    if (password.length === 0) {
        $("#register_pass_label").css({ 'color': 'red'});
        $("#invalid_registration_message").text("Your password cannot be empty!");
        return;
    }

    if (password.length < 8) {
        $("#register_pass_label").css({'color': 'red'});
        $("#invalid_registration_message").text("Your password must be at least 8 characters long!");
        return;
    }

    if (password !== confirmPass) {
        $("#register_pass_label").css({ 'color': 'red'});
        $("#confirm_label").css({ 'color': 'red'});
        $("#invalid_registration_message").text("Your passwords do not match!");
        return;
    }

    $.ajax({
        url: 'ajax/register.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            email: (email),
            password: (password)
        },
        success: function(data) {
	        if (data == 1) {
                console.log("Successful login!");
                alert("Registration successful! You may now login.");
                $("#register_email").val(""); // Clear the register email input
                $("#register_password").val(""); // Clear the register password input
                $("#confirm_password").val(""); // Clear the confirm password input
                $("#login_email").val(email); // Set the login email input to the registered input
            }
            else {
                console.log("Error: " + data); // For debugging issues with the query
                $("#register_email_label").css({ 'color': 'red'});
                $("#invalid_registration_message").text("This email has already been taken!");
            }
        },
        error: function() {
            console.log("Error with registering!");
        }
    });
}


// Clear all invalid input messages and label color changes
function clearInvalidInputNotifiers() {
    $("label").css({ 'color': 'black'});
    $("#invalid_registration_message").text("");
    $("#invalid_login_message").text("");
}

// Allows the user to send a message to the participants in that conversation
function sendMessage() {

    var text = $("#message_box").val(); // Get the message contents from the input
    var conversationId = $("chat_window").data("conversation_id"); // If it exists send to that conversation, otherwise start a new one
    var recipients = $("#recipients").val();

    if (text.length == 0) {
        alert("You cannot send an empty message!");
        return;
    }
    if (recipients.length == 0) {
        alert("You must have at least one other participant in the conversation!");
        return;
    }
    if (!recipients.includes("wku.edu")) {
        alert("The participants must be registered Topperchat users!");
        return;
    }
    if(text.length > 255) {
        var difference = text.length - 255;
        alert("Your message is " + difference + " characters too long (255 characters max)!");
        return;
    }
    if ($("#chat_window").is(':empty')) {
        $("#recipients").prop('disabled', true);
        $("#to_button").prop('disabled', true);
    }

    $.ajax({
        url: 'ajax/send_message.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            text: (text),
            recipients: (recipients)
        },
        success: function(data) {
            // For appending the conversation box
            try {
                var jData = JSON.parse(data);
            } catch(error) {
                alert(data);
                return;
            }

            if (jData["success"] == 1) {
                console.log("Appending conversation box to the conversations list!");
                var boxToAppend = jData["boxToAppend"];
                $("#conversations_list").append(boxToAppend);
                data = 1;
            }

            // For sending the message
            if (jData["messageId"] != null) {
                console.log("Sending message!");

                var messageId = jData["messageId"];

                // Create the message
                var message = $("<div data-id=" + messageId + " class='message message_from_self'><p>" + text + "</p></div>");

                // If this is the first message, add to the top
                if ($("#chat_window").children(".message_from_self").length == 0) {
                    $(message).hide().appendTo("#chat_window").fadeIn(700);
                }
                // Otherwise, add it after the last message
                else {
                    $(message).hide().insertAfter($("#chat_window").children(".message").last()).fadeIn(700);
                }

                // Clear the message box
                $("#message_box").val("");

                // Scroll to the bottom of the chat window
                $("#chat_window").animate({scrollTop: $("#chat_window").prop("scrollHeight")}, 'slow');

                // Update the recipients box to include the sender (if its the first message in the conversation
				if ($("#chat_window").is(':empty')) {
					$("#recipients").attr('value', jData["senderEmail"] + "; " + recipients);
					$("#recipients").val(jData["senderEmail"] + "; " + recipients);
				}
            }
            else {
                alert(data);
            }
        },
        error: function() {
            console.log("Error with sending message!");
        }
    });
}

// Allows the user to logout
function logout() {
    $.ajax({
        url: 'ajax/logout.ajax.php',
        type: 'POST',
        data: {
            AJAX: true
        },
        success: function() {
            console.log("Logging out!");
            window.location.reload(); // Reload the page now that the user id has been cleared from the session
        },
        error: function() {
            console.log("Error with logging out!");
        }
    });
}

// Allows the user to select a conversation
function selectConversation(element) {
    // Already selected so return
    if ($(element).hasClass("selected_conversation")) {
        return;
    }

    // Get the id associated with the clicked conversation box
    var conversationId = $(element).data("id");
    // Remove the selected class from the previously selected conversation
    $(".selected_conversation").removeClass("selected_conversation");

    $.ajax({
        url: 'ajax/select_conversation.ajax.php',
        type: 'POST',
        data: {
            AJAX: true,
            conversationId: (conversationId)
        },
        success: function(data) {
            console.log("Switching conversation!");
            $("body").empty().append(data);
            $("#recipients").prop('disabled', true);
            $("#to_button").prop('disabled', true);
			$("#chat_window").animate({scrollTop: $("#chat_window").prop("scrollHeight")}, 'slow');
            loadAllBackgroundFunctions();
        },
        error: function() {
            console.log("Error with switching conversation!");
        }
    });
}

// Allows the user to create a new conversation
function newConversation() {
    // Clear the session variable for the conversation id
    $.ajax({
        url: 'ajax/new_conversation.ajax.php',
        type: 'POST',
        data: {
            AJAX: true
        },
        success: function(data) {
            console.log("Creating new chat window for this new conversation!");
            $("body").empty().append("" + data);
            loadAllBackgroundFunctions();
        },
        error: function() {
            console.log("Error with creating new chat window for a new conversation!");
        }
    });
}

// Allows the user to view the popup for searching for other users
function showSearchUsersPopup() {
    console.log("Opening search popup...");
    $("<div id='search_clickout' onclick='closeSearchUsersPopup();'></div>").hide().appendTo("body").fadeIn(700);
    $("<div id='search_popup'><h2>Search for Users</h2><input id='search_box' type='text' placeholder='Start typing to search...'><div id='search_results'><p style='font-style:italic;'>Nothing to show</p></div></div>").hide().appendTo("body").fadeIn(700);
    addSearchBoxListener();
}

// Allows for an event to be bound to the dynamic content that shows on the search popup
function addSearchBoxListener() {
    // Catch typing so we can search the database
    $('#search_box').on('input', function() {
        // Get the input value
        var keyword = $("#search_box").val();

        if (keyword.length < 3) {
            // Empty the search results container
            $("#search_results").empty();
            // Update the search results to reflect the absence of matching records
            $("#search_results").append("<p class='empty_search_results_text'>Nothing to show</p>");
            return;
        }

        // Don't send another AJAX request if still searching already
        if (searching) {
            return;
        }

        // Update the search state
        searching = true;

        // Start the timer to delay searching again (0.8s between searches)
        searchTimer = setTimeout(function() {
            searching = false;
            clearTimeout(searchTimer);
            searchTimer = null;
        }, 800);

        // Search for matching user records
        console.log("Searching the database for matching user records...");
        $("#search_results p").text("Searching...");
        $.ajax({
            url: 'ajax/search_for_user.ajax.php',
            type: 'POST',
            data: {
                AJAX: true,
                keyword: keyword
            },
            success: function(data) {
                // Clear the search results div
                $("#search_results").empty();
                // If matching records were found
                if (data != 0) {
                    console.log("Found matching user records for the search!");
                    // Add the results to the search results div
                    $("#search_results").append(data);
                }
                else { // No matching records
                    console.log("No results for the user search.");
                    // Update the search results to reflect the absence of matching records
                    $("#search_results").append("<p class='empty_search_results_text'>Nothing to show</p>");
                }
            },
            error: function() {
                console.log("Error with searching for user!");
            }
        });
    });
}

// Allows the user to click outside of the search popup to close it
function closeSearchUsersPopup() {
    console.log("Closing search popup...");
    $("#search_box").unbind(); // Unbind all the events associated with the search box
    $("#search_clickout").fadeOut(700, function() {
        $("#search_clickout").detach();
    });
    $("#search_popup").fadeOut(700, function() {
        $("#search_popup").detach();
    });
}

function addParticipant(searchResult, currentUserEmail) {
    var email = $(searchResult).text();
    var recipientsText = $("#recipients").val();

    if (currentUserEmail == email) {
        alert("You are already a participant!");
        return;
    }

    if (recipientsText.length == 0 || (!recipientsText.includes("@wku.edu") && !recipientsText.includes("@topper.wku.edu"))) {
        console.log("Adding participant...");
        $("#recipients").val(email);
    }
    else if (recipientsText.includes(email)) {
        alert("This user has already been added as a recipient!");
    }
    else if ((recipientsText.match(new RegExp(";", "g")) || []).length > 4) {
        alert("You cannot have more than five other participants!");
    }
    else {
        console.log("Adding participant...");
        $("#recipients").val(recipientsText + "; " + email);
    }

}

